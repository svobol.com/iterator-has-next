const { extendIterator } = require('../dist/index')

/**
 * Creates iteratble iterator with given element count.
 * @param elementCount Positive whole number. Default value is 5
 */

async function * createIterator (elementCount = 5) {
  for (let i = 0; i < elementCount; i++) {
    yield i
  }
}

async function usage () {
  const iterator = createIterator(3)
  // const iterator = [1, 2, 3]
  const extendedIterator = await extendIterator(iterator)
  for await (let i of iterator) {
    console.log(`${i} | ${extendedIterator.hasNext()}`)
  }
}

usage().catch(reason => {
  console.error(reason)
  process.exit(1)
})
