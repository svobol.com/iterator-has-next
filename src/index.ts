
interface ExtendedAsyncIterableIterator<T> extends AsyncIterableIterator<T> {
  hasNext(): boolean
}

async function extendIterator<T> (iterator: AsyncIterableIterator<T>): Promise<ExtendedAsyncIterableIterator<T>> {
  let curr = await iterator.next()
  let next = await iterator.next()
  let hasNext = !next.done
  async function * innerIterator(): AsyncIterableIterator<T> {
    while (!curr.done) {
      yield curr.value
      curr = next
      next = await iterator.next()
      hasNext = !next.done
    }
  }
  const wrapper = innerIterator()
  Object.defineProperty(wrapper, 'hasNext', {
    writable: false,
    value: () => hasNext,
  })
  return wrapper as ExtendedAsyncIterableIterator<T>
}

export {
  extendIterator
}
