# iterator-has-next

Simple helper that wraps standard [AsyncIterableIterator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol/asyncIterator) and provides `.hasNext()` method.

Example code bellow or you can [try it on repl.it](https://repl.it/repls/YummyWrathfulRhombus).
```JavaScript
async function usage () {
  // Create dummy iterator with 3 elemnts
  const iterator = createIterator(3)
  // Extend iterator to get one with .hasNext() method
  const extendedIterator = await extendIterator(iterator)
  console.log(`Example #1 how utilize new method`)
  for await (let i of extendedIterator) {
    console.log(`${i} | ${extendedIterator.hasNext()}`)
  }
  console.log(`Example #2 of how to use .hasNext()`)
  const it = await extendIterator(createIterator(3))
  while (it.hasNext()) {
    const { value } = await it.next()
    console.log(`${value} | ${it.hasNext()}`)
  }
}
```
